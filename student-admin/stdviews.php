<?php

// connection to db
$db = new PDO('mysql:host=localhost;dbname=dbtest;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `registrations` ORDER BY id DESC";

//execute the query using php

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Views All Students</title>

    <!-- Bootstrap -->
    <link href="./boot/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
   table {
  border-collapse: separate;
  border-spacing: 4px;

}
table,
th,
td {
  border: 1px solid #cecfd5;
}
th,
td {
  padding: 10px 15px;
}
</style>
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div><a href="home.php"> Add New </a></div>
            <table class="table table-bordered">
               <thead>
                   <tr>
                       <th>ID</th>
                       <th>First Name</th>
                       <th>Last Name</th>
                       <th>Seip Id</th>
                        <th>Mobile Number</th>
                       <th>Images</th>
                       <th>Created At</th>
                       <th>Modiefied At</th>
                        <th>Action</th>
                       
                   </tr>
               </thead>
                <tbody>
                <?php

                //foreach ($db->query($query) as $row) {
                foreach ($db->query($query) as $registrations): ?>

                    <tr>
                        <td><?php echo $registrations['id'];?></td>
                        <td><?php echo $registrations['first_name'];?></td>
                        <td><?php echo $registrations['last_name'];?></td>
                        <td><?php echo $registrations['seip'];?></td>
                        <td><?php echo $registrations['mobile_num'];?></td>
                        <td><?php echo $registrations['images'];?></td>
                        <td><?php echo date("d/m/Y",strtotime($registrations['created_at']));?></td>
                        <td><?php echo $registrations['modified_at'];?></td>
                    <td> <a  href="std_show.php?id=<?php echo $registrations['id'];?>" class="btn btn-primary">Show</a>
                        | <a href="std_edit.php?id=<?php echo $registrations['id'];?>" class="btn btn-warning">Edit</a> |
                  <a href="std_delete.php?id=<?php echo $registrations['id'];?>" class="btn btn-danger">Delete</a></td>


                    </tr>
                <?php
                endforeach;
                //}
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>

