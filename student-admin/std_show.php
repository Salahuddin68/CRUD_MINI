<?php

// connection to db
$db = new PDO('mysql:host=localhost;dbname=dbtest;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `registrations` WHERE id = ".$_GET['id'];

//execute the query using php
foreach ($db->query($query) as $row){
    $registrations = $row;
}


?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Students Details</title>

    <!-- Bootstrap -->
    <link href="../boot/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div><a href="index.php"> Add New </a></div>
            <dl>
                <dt>id</dt>
                <dd><?php echo $registrations['id'];?></dd>

                <dt> first_name and last_name</dt>
                <dd><?php echo $registrations['first_name']." ".$registrations['last_name'];?></dd>

                <dt>seip</dt>
                <dd><?php echo $registrations['seip'];?></dd>


                <dt>Mobile_num</dt>
                <dd><?php echo $registrations['mobile_num'];?></dd>

                <dt>Image</dt>
                <dd><?php echo $registrations['images'];?></dd>


                <dt>Created</dt>
                <dd><?php echo date("d/m/Y", strtotime($registrations['created_at']));?></dd>

                <dt>Modified</dt>
                <dd><?php echo date("d/m/Y", strtotime($registrations['modified_at']));?></dd>
            </dl>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>