<!DOCTYPE html>
<html>
<head>
  <title></title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="">
</head>
<body>
<?php include 'header.php'; ?>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<?php include'sidebar.php'; ?>
		  </div>
		  <div class="col-md-10">
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="content-box-large">
		  				<div class="panel-heading">
							<div class="panel-title">Add Information: </div>
							<hr>
							<form method="POST" action="connect.php">
							  <div class="form-group">
							    <label for="fname">First Name</label>
							    <input type="text" class="form-control" id="fname" name="fname" placeholder="Enter Your First Name" autofocus="">
							  </div>
							  <div class="form-group">
							    <label for="lname">Last Name</label>
							    <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter Your Last Name">
							  </div>

							   <div class="form-group">
							    <label for="seip">Seip ID</label>
							    <input type="number" class="form-control" id="seip" name="seip" placeholder="Enter Your Seip ID">
							  </div>


							  <div class="form-group">
							    <label for="mnum">Mobile Num:</label>
							    <input type="number" class="form-control" name="mnum" id="mnum" placeholder="Enter Your Mobile Number">
							  </div>
							  <div class="form-group">
							    <label for="finput">File input</label>
							    <input type="file" id="finput" name="finput">
							  </div>
							  <div class="checkbox">
							    <label>
							      <input type="checkbox"> Check me out
							    </label>
							  </div>
							  <button type="submit" name="submit" class="btn btn-default">Submit</button>
							</form>
						</div>
		  				
		  			</div>
		  		</div>

		  		
		  	</div>
            
            </div>
            </div></div>
<?php include 'footer.php'; ?>

</body>
</html>
		  	
		  

